package ru.meseen.dev.stock.data.network.pojo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class CompanyProfileResponse(

	@SerialName("finnhubIndustry")
	val finnhubIndustry: String? = null,

	@SerialName("country")
	val country: String? = null,

	@SerialName("ticker")
	val ticker: String? = null,

	@SerialName("marketCapitalization")
	val marketCapitalization: Double? = null,

	@SerialName("phone")
	val phone: String? = null,

	@SerialName("weburl")
	val weburl: String? = null,

	@SerialName("name")
	val name: String? = null,

	@SerialName("ipo")
	val ipo: String? = null,

	@SerialName("logo")
	val logo: String? = null,

	@SerialName("currency")
	val currency: String? = null,

	@SerialName("exchange")
	val exchange: String? = null,

	@SerialName("shareOutstanding")
	val shareOutstanding: Double? = null
)
